from django.forms import models, Textarea

from ask.models import Question


class QuestionForm(models.ModelForm):
    class Meta:
        model = Question
        fields = ['question']
        widgets = {'question': Textarea(attrs={'rows': 8})}