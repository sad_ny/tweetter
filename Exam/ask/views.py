from django.contrib.auth.views import LoginView
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import ListView, FormView

from ask.forms import QuestionForm
from ask.models import Answer, Question, AskUser


class AskLogin(LoginView):
    template_name = 'login.html'

    def get_success_url(self):
        username = self.kwargs.get('username', 'NoOne')
        return reverse('profile', kwargs={'username': username})


class AskProfileView(ListView):
    template_name = 'profile.html'
    model = Answer
    context_object_name = 'all_answers'

    def get_queryset(self):
        profile = get_object_or_404(AskUser, username=self.kwargs['username'])
        lst_q = Question.objects.filter(to_user=profile)
        lst_a = Answer.objects.union(lst_q).values('question', 'answer')
        return lst_a

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        auth = True if user.is_authenticated else False
        context['profile_username'] = self.kwargs['username']
        context['user'] = user
        context['auth'] = auth
        return context


class AskView(FormView):
    template_name = 'ask.html'
    form_class = QuestionForm

    def form_valid(self, form):
        username = AskUser.objects.get(self.kwargs['username'])
        form.instance(user_to=username)
        form.instance.save()
        return super(AskView, self).form_valid(form)

    def get_success_url(self):
        username = self.kwargs.get('username', 'NoOne')
        return reverse('profile', kwargs={'username': username})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['profile_username'] = self.kwargs['username']
        return context


