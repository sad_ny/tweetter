from django.urls import path, re_path
from .views import *


urlpatterns = [
    path('login', AskLogin.as_view(), name='login'),
    re_path(r"^(?P<username>[\w.@+-]+)$", AskProfileView.as_view(), name='profile'),
    re_path(r"^(?P<username>[\w.@+-]+)/ask$", AskView.as_view(), name='ask'),
    # re_path(r"^(?P<username>[\w.@+-]+)/questions$",,name='questions')
]
