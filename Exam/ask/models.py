from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models import CASCADE


class AskUser(AbstractUser):
    def __str__(self):
        return self.get_full_name()


class Question(models.Model):
    question = models.CharField(max_length=300)
    to_user = models.ForeignKey(AskUser, on_delete=CASCADE, related_name="all_questions")


class Answer(models.Model):
    answer = models.CharField(max_length=300)
    question = models.OneToOneField(Question, on_delete=CASCADE)
    like_count = models.PositiveIntegerField()
