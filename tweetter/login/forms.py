from django.core.validators import RegexValidator
from django.forms import ModelForm, Form, CharField, PasswordInput, FileInput, DateInput, Textarea
from login.models import TweetterUser


class TweetterUserForm(ModelForm):
    username = CharField(max_length=30, validators=[RegexValidator(regex='^[A-Za-z0-9_]+$', message="Only "
                                                                                                    "alphanumeric "
                                                                                                    "symbols (include "
                                                                                                    "underscore).")])

    class Meta:
        model = TweetterUser
        fields = ['username', 'name', 'bio', 'birth_date', 'photo', 'password']
        widgets = {
            'bio': Textarea(attrs={'rows': 4}),
            'birth_date': DateInput(attrs={'placeholder': 'yyyy-mm-dd'}),
            'photo': FileInput,
            'password': PasswordInput
        }
