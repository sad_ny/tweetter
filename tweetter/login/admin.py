from django.contrib import admin

from login.models import TweetterUser

admin.site.register(TweetterUser)