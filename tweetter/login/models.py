from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.db import models
from django.contrib.auth.models import AbstractUser


class TweetterUser(AbstractUser):
    name = models.CharField(max_length=50)
    bio = models.CharField(max_length=160, null=True, blank=True)
    birth_date = models.DateField()
    photo = models.ImageField(upload_to="user_profile_image", null=True, blank=True)
    joined = models.DateField(auto_now_add=True)

    def __init__(self, *args, **kwargs):
        super(TweetterUser, self).__init__(*args, **kwargs)
        self._meta.get_field('username').validators = [RegexValidator(regex='^[A-Za-z0-9_]{1,30}$', message="Only "
                                                                                                            "alphanumeric "
                                                                                                            "symbols.")]

    def __str__(self):
        return f"{self.name}(@{self.username})"
