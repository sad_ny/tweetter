from django.contrib.auth.views import LoginView, logout_then_login
from django.urls import reverse_lazy, reverse
from django.views.generic import FormView
from django.contrib.auth.models import Group

from login.forms import TweetterUserForm
from login.models import TweetterUser


class SignUpView(FormView):
    template_name = 'sign_up.html'
    form_class = TweetterUserForm

    def form_valid(self, form):
        instance = form.save(commit=False)
        user = TweetterUser.objects.create_user(username=instance.username, password=instance.password,
                                                name=instance.name,
                                                birth_date=instance.birth_date, photo=instance.photo)
        group = Group.objects.get(name='Casual users')
        user.groups.add(group)
        return super(SignUpView, self).form_valid(form)

    def get_success_url(self):
        return reverse('login')


class TweetterLoginView(LoginView):
    template_name = 'login.html'
    redirect_authenticated_user = True

    def get_redirect_url(self):
        return reverse_lazy('home')


def TweetterLogout(request):
    return logout_then_login(request, 'login')
