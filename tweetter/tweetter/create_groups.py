from django.contrib.auth.models import Group, Permission


def create_groups():
    print("Creating user groups with permissions...", end=" ")

    users = Group.objects.create(name='Casual users')
    staff = Group.objects.create(name='Admin staff')

    permissions = Permission.objects.filter(codename__contains='tweet')

    for permission in permissions:
        if permission.name.__contains__('tweet'):
            if permission.codename.__contains__('change') or permission.codename.__contains__('delete'):
                staff.permissions.add(permission)
            else:
                users.permissions.add(permission)
                staff.permissions.add(permission)

    print("OK")
