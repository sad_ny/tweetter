from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import auth
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView
from django.forms import modelform_factory, Textarea
from django.views.generic.edit import FormMixin
from django.db.models import Q, F

from simple_tweetter.forms import TweetForm
from login.models import TweetterUser
from simple_tweetter.models import Tweet, Followers, UserMention, Hashtag, TweetHashtag, Url, TweetUrl, Like


def start_page(request):
    if request.method == 'GET':
        user = auth.get_user(request)
        if user.is_authenticated:
            return redirect(reverse('home'))
        else:
            return redirect(reverse('login'))


class HomePageView(LoginRequiredMixin, FormMixin, ListView):
    form_class = modelform_factory(Tweet, form=TweetForm,
                                   widgets={'tweet': Textarea(attrs={'rows': 4, 'placeholder': "What's happening?",
                                                                     'onkeyup': "countChar(this)"})})
    login_url = reverse_lazy('login')
    template_name = 'home_page.html'
    model = Tweet
    context_object_name = 'all_tweets'

    def get_queryset(self):
        user = self.request.user
        if user is not None:
            if Followers.objects.filter(followers=user).exists():
                following_tweets_users = TweetterUser.objects.filter(username__in=user.following.all())
            else:
                following_tweets_users = TweetterUser.objects.none()
            all_tweets_users = following_tweets_users | TweetterUser.objects.filter(username=user.username)
            all_tweets = Tweet.objects.filter(user__in=all_tweets_users).order_by('-date')
            return all_tweets


def add_tweet(request):
    if request.method == "POST":
        form_class = modelform_factory(Tweet, form=TweetForm,
                                       widgets={'tweet': Textarea(attrs={'rows': 4, 'placeholder': "What's happening?",
                                                                         'onkeyup': "countChar(this)"})})
        form = form_class(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            text = instance.tweet
            user = request.user
            tweet = Tweet.objects.create(user=user, tweet=text)
            words = text.split()
            for word in words:
                if word[0] == '@':
                    mention = TweetterUser.objects.get(username=word[1:])
                    if mention is not None:
                        UserMention.objects.create(user=mention, tweet=tweet)
                elif word[0] == '#':
                    hashtag = Hashtag.objects.get_or_create(hashtag=word[1:])
                    TweetHashtag.objects.create(hashtag=hashtag, tweet=tweet)
                elif '.' in word:
                    url = Url.objects.get_or_create(url=word)
                    TweetUrl.objects.create(url=url, tweet=tweet)
        return redirect(reverse('home'))


class ProfileView(LoginRequiredMixin, DetailView):
    login_url = reverse_lazy('login')
    template_name = 'profile.html'
    model = TweetterUser

    def get_object(self, queryset=None):
        return get_object_or_404(TweetterUser, username=self.kwargs['username'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = TweetterUser.objects.get(username=self.kwargs['username'])
        context['me'] = True if user == self.request.user else False
        context['status'] = 'Follow'
        if Followers.objects.filter(followers=self.request.user).exists():
            if self.request.user.following.filter(following=user).exists():
                context['status'] = 'Following'
        if Tweet.objects.filter(user=user).exists():
            context['all_tweets'] = user.tweets.all()
        else:
            context['all_tweets'] = []
        if Followers.objects.filter(followers=user).exists():
            context['following'] = user.following.all().count()
        else:
            context['following'] = 0
        if Followers.objects.filter(following=user).exists():
            context['followers'] = user.followers.all().count()
        else:
            context['followers'] = 0
        return context


class FollowingView(LoginRequiredMixin, ListView):
    login_url = reverse_lazy('login')
    template_name = 'tweetter_users.html'
    context_object_name = 'users'

    def get_queryset(self):
        user = TweetterUser.objects.get(username=self.kwargs['username'])
        if Followers.objects.filter(followers=user).exists():
            return user.following.all()
        else:
            return TweetterUser.objects.none()

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['profile'] = self.kwargs['username']
        context['type'] = 'Following'
        return context


class FollowersView(LoginRequiredMixin, ListView):
    login_url = reverse_lazy('login')
    template_name = 'tweetter_users.html'
    context_object_name = 'users'

    def get_queryset(self):
        user = TweetterUser.objects.get(username=self.kwargs['username'])
        if Followers.objects.filter(following=user).exists():
            return user.followers.all()
        else:
            return TweetterUser.objects.none()

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['profile'] = self.kwargs['username']
        context['type'] = 'Followers'
        return context


class SearchView(LoginRequiredMixin, ListView):
    login_url = reverse_lazy('login')
    template_name = 'search.html'
    context_object_name = 'users'

    def get_queryset(self):
        req = self.request.GET['search']
        q = None
        if req[0] == "@":
            q1 = Q(username__exact=req[1:])
            q2 = Q(username__contains=req[1:])
            q = q & (q1 | q2) if q else q1 | q2
            return TweetterUser.objects.filter(q)
        elif req[0] == "#":
            return TweetterUser.objects.none()
        else:
            q1 = Q(username__exact=req)
            q2 = Q(username__contains=req)
            q = q & (q1 | q2) if q else q1 | q2
            return TweetterUser.objects.filter(q)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        req = self.request.GET['search']
        if req[0] == "@":
            user = TweetterUser.objects.get(username=req[1:])
            if user is not None:
                if UserMention.objects.filter(user=user).exists():
                    context['all_tweets'] = user.tweets_with_mentions.all()
            else:
                context['all_tweets'] = []
        elif req[0] == "#":
            hashtag = Hashtag.objects.get(hashtag=req[1:])
            if hashtag is None:
                q = Q(hashtag__contains=req[1:])
                context['all_tweets'] = Tweet.objects.filter(tweet__in=TweetHashtag.objects.filter(q))
            else:
                q = hashtag.tweets_with_hashtags.all()
                q1 = Q(hashtag__contains=req[1:])
                q1 = Tweet.objects.filter(tweet__in=TweetHashtag.objects.filter(q1))
                context['all_tweets'] = q | q1
        else:
            q = Q(tweet__contains=req)
            context['all_tweets'] = Tweet.objects.filter(q)

        return context


def like(request, tweet_id):
    if request.method == 'POST':
        tweet = Tweet.objects.get(pk=tweet_id)
        user = request.user
        if Like.objects.filter(tweet=tweet).exists():
            if tweet.who_liked.filter(user=user).exists():
                tweet.like_count = F('like_count') - 1
                tweet.save()
                Like.objects.filter(user=user, tweet=tweet).delete()
            else:
                tweet.like_count = F('like_count') + 1
                tweet.save()
                Like.objects.create(user=user, tweet=tweet)
        else:
            Like.objects.create(user=user, tweet=tweet)
            tweet.like_count = F('like_count') + 1
            tweet.save()
        tweet.refresh_from_db()
        return JsonResponse({'like_count': tweet.like_count})
    return JsonResponse({'like_count': 100500})


def follow(request, username):
    if request.method == 'POST':
        user = request.user
        follow_target = TweetterUser.objects.get(username=username)
        if Followers.objects.filter(following=follow_target).exists():
            if user.following.filter(following=follow_target).exists():
                follow_count = follow_target.followers.all().count() - 1
                Followers.objects.filter(followers=user, following=follow_target).delete()
                return JsonResponse({'status': 'Follow', 'follow_count' : follow_count})
            else:
                follow_count = follow_target.followers.all().count() + 1
                Followers.objects.create(followers=user, following=follow_target)
                return JsonResponse({'status': 'Following', 'follow_count' : follow_count})
        else:
            follow_count = 1
            Followers.objects.create(followers=user, following=follow_target)
            return JsonResponse({'status': 'Following', 'follow_count' : follow_count})
    return JsonResponse({'status': "Follow ing", 'follow_count' : 100500})