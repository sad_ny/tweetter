from django.db import models
from django.db.models import CASCADE

from login.models import TweetterUser


class Followers(models.Model):
    followers = models.ForeignKey(TweetterUser, on_delete=CASCADE, related_name='following')
    following = models.ForeignKey(TweetterUser, on_delete=CASCADE, related_name='followers')


class Url(models.Model):
    url = models.CharField(max_length=30)


class Hashtag(models.Model):
    hashtag = models.CharField(max_length=30)


class Tweet(models.Model):
    user = models.ForeignKey(TweetterUser, on_delete=CASCADE, related_name='tweets')
    tweet = models.CharField(max_length=140)
    date = models.DateField(auto_now_add=True)
    like_count = models.PositiveIntegerField(default=0)


class UserMention(models.Model):
    user = models.OneToOneField(TweetterUser, on_delete=CASCADE, related_name='tweets_with_mentions')
    tweet = models.ForeignKey(Tweet, on_delete=CASCADE, related_name='user_mentions_in_tweet')


class TweetUrl(models.Model):
    tweet = models.ForeignKey(Tweet, on_delete=CASCADE, related_name='urls')
    url = models.ForeignKey(Url, on_delete=CASCADE)


class TweetHashtag(models.Model):
    tweet = models.ForeignKey(Tweet, on_delete=CASCADE, related_name='hashtags')
    hashtag = models.ForeignKey(Hashtag, on_delete=CASCADE, related_name='tweets_with_hashtags')


class Like(models.Model):
    user = models.ForeignKey(TweetterUser, on_delete=CASCADE, related_name='liked_tweets')
    tweet = models.ForeignKey(Tweet, on_delete=CASCADE, related_name='who_liked')
