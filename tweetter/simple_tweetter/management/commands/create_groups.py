from django.core.management.base import BaseCommand
from tweetter.create_groups import create_groups


class Command(BaseCommand):
    def handle(self, *args, **options):
        create_groups()