from django.apps import AppConfig


class SimpleTweetterConfig(AppConfig):
    name = 'simple_tweetter'
