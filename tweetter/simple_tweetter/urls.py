from django.urls import path,re_path
from .views import *

urlpatterns = [
    path('', start_page, name='start page'),
    path('home', HomePageView.as_view(), name='home'),
    path('home/add_tweet', add_tweet, name='add_tweet'),
    path('search', SearchView.as_view(), name='search'),
    path('<int:tweet_id>/like', like, name='like'),
    re_path(r'^(?P<username>\w+)$', ProfileView.as_view(), name='profile'),
    re_path(r'^(?P<username>\w+)/follow$', follow, name='follow'),
    re_path(r'^(?P<username>\w+)/following$', FollowingView.as_view(), name='following'),
    re_path(r'^(?P<username>\w+)/followers$', FollowersView.as_view(), name='followers')
]