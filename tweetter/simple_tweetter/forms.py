from django.forms import modelform_factory
from simple_tweetter.models import Tweet

TweetForm = modelform_factory(Tweet, fields=('tweet',))
